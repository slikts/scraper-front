module Data.Item exposing (..)

import Data.Source exposing (SourceGroupDict, Group, Source)
import Date exposing (Date)
import Dict exposing (Dict)


type alias Item =
    { name : String
    , key : String
    , source : Source
    , group : Group
    , time : Date
    }


upd : Group -> Maybe (List Group) -> Maybe (List Group)
upd a b =
    a
        :: (Maybe.withDefault [] b)
        |> Just



-- toSourceDataDict : List Item -> SourceGroupDict
-- toSourceDataDict =
--     List.foldl (\{ source, group } b -> Dict.update source (upd group) b) Dict.empty
