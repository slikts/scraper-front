module Data.Source exposing (..)

import Dict exposing (Dict)
import Graphqelm.Http
import Json.Decode as Decode
import Json.Encode exposing (Value, bool, encode, float, int, list, object, string)
import RemoteData exposing (RemoteData)
import ScraperData.Scalar
import Set exposing (Set)


type alias Source =
    String


type alias Group =
    String


type alias SourceGroupPair =
    ( Source, Group )


type alias SubscriptionSet =
    Set SourceGroupPair


type alias SourceData =
    RemoteData Graphqelm.Http.Error SubscriptionSet


type alias SourceGroupDict =
    Dict Source (Set Group)


upd : Group -> Maybe (Set Group) -> Maybe (Set Group)
upd group set =
    Set.insert group (Maybe.withDefault Set.empty set)
        |> Just


toDict : SubscriptionSet -> SourceGroupDict
toDict =
    Set.foldl (\( name, group ) b -> Dict.update name (upd group) b) Dict.empty


checkIfSubbed : SourceGroupDict -> Source -> Group -> Bool
checkIfSubbed subs source group =
    Dict.get source subs
        |> Maybe.withDefault Set.empty
        |> Set.member group


decodeItem : Decode.Decoder SourceGroupPair
decodeItem =
    Decode.map2 (,) (Decode.index 0 Decode.string) (Decode.index 1 Decode.string)


decodeSubscription : ScraperData.Scalar.Json -> SubscriptionSet
decodeSubscription (ScraperData.Scalar.Json a) =
    Decode.decodeString (Decode.list decodeItem) a
        |> Result.withDefault []
        |> Set.fromList


encodeSubscriptionSet : SubscriptionSet -> ScraperData.Scalar.Json
encodeSubscriptionSet data =
    Set.toList data
        |> List.map (\( name, group ) -> list [ string name, string group ])
        |> list
        |> Json.Encode.encode 0
        |> ScraperData.Scalar.Json
