module Data.Subscription exposing (..)

import Data.Source exposing (SourceData, Source, Group, SourceGroupPair)
import UrlParser
import Set exposing (Set)


type alias Subscription =
    { id : UUID
    , data : SourceData
    }


type UUID
    = UUID String


idParser : UrlParser.Parser (String -> a) a
idParser =
    UrlParser.custom "UUID" Ok
