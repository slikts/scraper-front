module Main exposing (..)

import Html exposing (Html, a, div, h1, h2, img, p, pre, text)
import Json.Decode as Decode exposing (Value)
import Navigation exposing (Location)
import Page.Edit
import Page.Errored exposing (PageLoadError)
import Page.NotFound
import Page.Subscription
import Route exposing (Route)
import Task
import Util exposing ((=>))
import Views.Edit
import Views.Page
import Views.Subscription


type Msg
    = SetRoute (Maybe Route)
    | NewLoaded (Result PageLoadError Page.Edit.Model)
    | EditLoaded (Result PageLoadError Page.Edit.Model)
    | SubscriptionLoaded (Result PageLoadError Page.Subscription.Model)
    | EditMsg Page.Edit.Msg
    | SubscriptionMsg Page.Subscription.Msg


type Page
    = Blank
    | NotFound
    | Errored Page.Errored.PageLoadError
    | Edit Page.Edit.Model
    | Subscription Page.Subscription.Model


type PageState
    = Loaded Page
    | TransitioningFrom Page


type alias Model =
    { pageState : PageState
    }


pageErrored : Model -> String -> ( Model, Cmd msg )
pageErrored model errorMessage =
    let
        error =
            Page.Errored.pageLoadError errorMessage
    in
    { model | pageState = Loaded (Errored error) } => Cmd.none


getPage : PageState -> Page
getPage pageState =
    case pageState of
        Loaded page ->
            page

        TransitioningFrom page ->
            page


setRoute : Maybe Route -> Model -> ( Model, Cmd Msg )
setRoute maybeRoute model =
    let
        transition toMsg task =
            { model | pageState = TransitioningFrom (getPage model.pageState) }
                => Task.attempt toMsg task

        errored =
            pageErrored model
    in
    case maybeRoute of
        Nothing ->
            { model | pageState = Loaded NotFound } => Cmd.none

        Just Route.New ->
            transition NewLoaded (Page.Edit.init "")

        Just (Route.Edit id) ->
            transition EditLoaded (Page.Edit.init id)

        Just (Route.Subscription id) ->
            transition SubscriptionLoaded (Page.Subscription.init id)


view : Model -> Html Msg
view model =
    case model.pageState of
        Loaded page ->
            viewPage False page

        TransitioningFrom page ->
            viewPage True page


viewPage : Bool -> Page -> Html Msg
viewPage isLoading page =
    let
        frame =
            Views.Page.frame isLoading
    in
    case page of
        NotFound ->
            Page.NotFound.view
                |> frame Views.Page.Other

        Blank ->
            Html.text ""
                |> frame Views.Page.Other

        Errored subModel ->
            Page.Errored.view subModel
                |> frame Views.Page.Other

        Edit subModel ->
            Views.Edit.view subModel
                |> frame Views.Page.Edit
                |> Html.map EditMsg

        Subscription subModel ->
            Views.Subscription.view subModel
                |> frame Views.Page.Subscription
                |> Html.map SubscriptionMsg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    updatePage (getPage model.pageState) msg model


updatePage : Page -> Msg -> Model -> ( Model, Cmd Msg )
updatePage page msg model =
    let
        toPage toModel toMsg subUpdate subMsg subModel =
            let
                ( newModel, newCmd ) =
                    subUpdate subMsg subModel
            in
            ( { model | pageState = Loaded (toModel newModel) }, Cmd.map toMsg newCmd )

        errored =
            pageErrored model
    in
    case ( msg, page ) of
        ( SetRoute route, _ ) ->
            setRoute route model

        ( EditLoaded (Ok subModel), _ ) ->
            { model | pageState = Loaded (Edit subModel) } => Cmd.none

        ( EditLoaded (Err error), _ ) ->
            { model | pageState = Loaded (Errored error) } => Cmd.none

        ( NewLoaded (Ok subModel), _ ) ->
            { model | pageState = Loaded (Edit subModel) } => Cmd.none

        ( NewLoaded (Err error), _ ) ->
            { model | pageState = Loaded (Errored error) } => Cmd.none

        ( SubscriptionLoaded (Ok subModel), _ ) ->
            { model | pageState = Loaded (Subscription subModel) } => Cmd.none

        ( SubscriptionLoaded (Err error), _ ) ->
            { model | pageState = Loaded (Errored error) } => Cmd.none

        ( EditMsg subMsg, Edit subModel ) ->
            toPage Edit EditMsg Page.Edit.update subMsg subModel

        ( SubscriptionMsg Page.Subscription.EditMsg, _ ) ->
            model => Cmd.none

        ( SubscriptionMsg Page.Subscription.RefreshMsg, _ ) ->
            model => Cmd.none

        ( _, NotFound ) ->
            model => Cmd.none

        ( _, _ ) ->
            model => Cmd.none


pageSubscriptions : Page -> Sub Msg
pageSubscriptions page =
    case page of
        Blank ->
            Sub.none

        Errored _ ->
            Sub.none

        NotFound ->
            Sub.none

        Edit _ ->
            Sub.none

        Subscription _ ->
            Sub.none

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ pageSubscriptions (getPage model.pageState)
        ]


main : Program Value Model Msg
main =
    Navigation.programWithFlags (Route.fromLocation >> SetRoute)
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


initialPage : Page
initialPage =
    Blank


init : Value -> Location -> ( Model, Cmd Msg )
init val location =
    setRoute (Route.fromLocation location)
        { pageState = Loaded initialPage
        }
