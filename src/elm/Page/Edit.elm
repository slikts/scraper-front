module Page.Edit exposing (..)

import Data.Source exposing (Group, Source, SourceData, SourceGroupDict, SourceGroupPair, SubscriptionSet)
import Data.Subscription exposing (Subscription, UUID(..))
import Graphqelm.Http exposing (Error(..))
import Page.Errored exposing (PageLoadError, pageLoadError)
import Request exposing (query)
import Request.Source
import Request.Subscription
import Route
import Set exposing (Set)
import Task exposing (Task)
import Util exposing ((=>))


type alias Model =
    { id : String
    , visibleSources : Set Source
    , data : SubscriptionSet
    , subs : SubscriptionSet
    }


type Msg
    = Create
    | Created UUID
    | Update
    | Updated
    | ToggleSub SourceGroupPair Bool
    | Error Graphqelm.Http.Error
    | ToggleSource Source


loadSourceData : Task Graphqelm.Http.Error SubscriptionSet
loadSourceData =
    Request.query Request.Source.get


loadSubs : String -> Task Graphqelm.Http.Error SubscriptionSet
loadSubs id =
    Request.query (Request.Subscription.getSubscription (UUID id))


init : String -> Task PageLoadError Model
init id =
    let
        handleLoadError a =
            pageLoadError (Basics.toString a)
    in
    Task.map2 (Model id Set.empty) loadSourceData (loadSubs id)
        |> Task.mapError handleLoadError


updateSubs : Bool -> SourceGroupPair -> SubscriptionSet -> SubscriptionSet
updateSubs checked =
    if checked then
        Set.insert
    else
        Set.remove


created : Result Error (Maybe String) -> Msg
created a =
    case a of
        Ok id ->
            Created (UUID (Maybe.withDefault "" id))

        Err err ->
            Error err


updated : Result Error value -> Msg
updated a =
    case a of
        Ok _ ->
            Updated

        Err err ->
            Error err


toggleSetItem : comparable -> Set comparable -> Set comparable
toggleSetItem item set =
    if Set.member item set then
        Set.remove item set
    else
        Set.insert item set


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Create ->
            if Set.isEmpty model.subs then
                model => Cmd.none
            else
                model => Task.attempt created (Request.mutation (Request.Subscription.create model.subs))

        Created (UUID id) ->
            model => Route.modifyUrl (Route.Subscription id)

        Update ->
            if Set.isEmpty model.subs then
                model => Cmd.none
            else
                model => Task.attempt updated (Request.mutation (Request.Subscription.update (UUID model.id) model.subs))

        Updated ->
            model => Route.modifyUrl (Route.Subscription model.id)

        Error err ->
            let
                z =
                    Debug.log "error" err
            in
            model => Cmd.none

        ToggleSub ( source, group ) checked ->
            (,) { model | subs = updateSubs checked ( source, group ) model.subs } Cmd.none

        ToggleSource source ->
            { model | visibleSources = toggleSetItem source model.visibleSources } => Cmd.none
