module Page.Subscription exposing (..)

import Data.Item exposing (Item)
import Data.Subscription exposing (UUID(..))
import Date exposing (Date)
import Date.Format
import Html exposing (..)
import Html.Attributes exposing (attribute, class, classList, href, id, placeholder, rel, src)
import Page.Errored exposing (PageLoadError, pageLoadError)
import Request exposing (query)
import Request.Item
import Route
import Task exposing (Task)


type alias Model =
    { id : String
    , data : List Item
    }


type Msg
    = EditMsg
    | RefreshMsg


init : String -> Task PageLoadError Model
init id =
    let
        handleLoadError a =
            pageLoadError (Basics.toString a)
    in
    Task.map (Model id) (Request.query (Request.Item.getBySub (UUID id)))
        |> Task.mapError handleLoadError
