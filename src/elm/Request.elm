module Request exposing (..)

-- import RemoteData exposing (RemoteData)

import Graphqelm.Http
import Graphqelm.Operation exposing (RootMutation, RootQuery)
import Graphqelm.SelectionSet exposing (SelectionSet)
import Task exposing (Task)


-- import Page.Errored exposing (PageLoadError, pageLoadError)


apiUrl : String
apiUrl =
    "http://localhost:5000/graphql"


query :
    SelectionSet decodesTo RootQuery
    -> Task Graphqelm.Http.Error decodesTo
query =
    Graphqelm.Http.queryRequest apiUrl >> Graphqelm.Http.toTask


mutation :
    SelectionSet decodesTo RootMutation
    -> Task Graphqelm.Http.Error decodesTo
mutation =
    Graphqelm.Http.mutationRequest apiUrl >> Graphqelm.Http.toTask



-- |> Graphqelm.Http.send (RemoteData.fromResult >> msg)
