module Request.Item exposing (..)

import Data.Item exposing (Item)
import ScraperData.Object.Item as Item
import ScraperData.Query as Query
import ScraperData.Object.GetSubItemsConnection as GetSubItemsConnection
import ScraperData.Object
import Graphqelm.Operation exposing (RootQuery)
import Graphqelm.OptionalArgument exposing (OptionalArgument(Null, Present))
import Graphqelm.SelectionSet exposing (SelectionSet, with)
import Graphqelm.Field as Field
import RemoteData exposing (RemoteData)
import Graphqelm.Http
import ScraperData.Scalar
import Date exposing (Date)
import Data.Subscription exposing (UUID(..))


type alias ItemResponse =
    List Item


type alias ItemData =
    RemoteData Graphqelm.Http.Error ItemResponse


getBySub : UUID -> SelectionSet (List Item) RootQuery
getBySub (UUID subId) =
    Query.selection identity
        |> with
            ((Query.getSubItems
                (\a -> { a | subid = Present subId })
                nodesSelection
             )
                |> Field.nonNullOrFail
            )


nodesSelection : SelectionSet (List Item) ScraperData.Object.GetSubItemsConnection
nodesSelection =
    GetSubItemsConnection.selection identity
        |> with
            ((GetSubItemsConnection.nodes itemSelection)
                |> Field.map (Maybe.withDefault [])
                |> Field.nonNullElementsOrFail
            )


toDate : ScraperData.Scalar.Datetime -> Date
toDate (ScraperData.Scalar.Datetime a) =
    Result.withDefault (Date.fromTime 0) (Date.fromString a)


itemSelection : SelectionSet Item ScraperData.Object.Item
itemSelection =
    Item.selection Item
        |> with
            Item.name
        |> with
            Item.key
        |> with
            Item.source
        |> with
            Item.group
        |> with
            (Item.time
                |> Field.map toDate
            )
