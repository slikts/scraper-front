module Request.Source exposing (..)

import Data.Source exposing (SourceGroupPair, SubscriptionSet)
import ScraperData.Object.Sourcedatum as Sourcedatum
import ScraperData.Query as Query
import ScraperData.Object exposing (SourcedataConnection, Sourcedatum)
import ScraperData.Object.SourcedataConnection as SourcedataConnection
import Graphqelm.SelectionSet exposing (SelectionSet, with)
import Graphqelm.Field as Field
import Graphqelm.Operation exposing (RootQuery)
import Set exposing (Set)


get : SelectionSet SubscriptionSet RootQuery
get =
    Query.selection identity
        |> with
            ((Query.allSourcedata
                identity
                nodesSelection
             )
                |> Field.nonNullOrFail
            )


nodesSelection : SelectionSet SubscriptionSet SourcedataConnection
nodesSelection =
    SourcedataConnection.selection Set.fromList
        |> with
            ((SourcedataConnection.nodes itemSelection)
                |> Field.nonNullOrFail
            )


itemSelection : SelectionSet SourceGroupPair Sourcedatum
itemSelection =
    Sourcedatum.selection (,)
        |> with
            (Sourcedatum.name
                |> Field.nonNullOrFail
            )
        |> with
            (Sourcedatum.group
                |> Field.nonNullOrFail
            )
