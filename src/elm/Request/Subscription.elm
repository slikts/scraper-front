module Request.Subscription exposing (..)

-- import Graphqelm.Field as Field

import Data.Source exposing (SubscriptionSet, decodeSubscription, encodeSubscriptionSet)
import Data.Subscription exposing (UUID(..))
import Graphqelm.Operation exposing (RootMutation, RootQuery)
import Graphqelm.OptionalArgument exposing (OptionalArgument(Absent, Null, Present))
import Graphqelm.SelectionSet exposing (SelectionSet, with)
import Maybe.Extra
import ScraperData.Mutation as Mutation
import ScraperData.Object exposing (CreateSubscriptionPayload, UpdateSubscriptionPayload)
import ScraperData.Object.CreateSubscriptionPayload as CreateSubscriptionPayload
import ScraperData.Object.Subscription as Subscription
import ScraperData.Object.UpdateSubscriptionPayload as UpdateSubscriptionPayload
import ScraperData.Query as Query
import Set exposing (Set)


getSubscription :
    UUID
    -> SelectionSet SubscriptionSet RootQuery
getSubscription (UUID id) =
    Query.selection (Maybe.withDefault Set.empty)
        |> with
            (Query.subscriptionById { id = id }
                subscriptionGetSelection
            )


subscriptionGetSelection : SelectionSet SubscriptionSet ScraperData.Object.Subscription
subscriptionGetSelection =
    Subscription.selection decodeSubscription
        |> with
            Subscription.data


create : SubscriptionSet -> SelectionSet (Maybe String) RootMutation
create data =
    Mutation.selection Maybe.Extra.join
        |> with
            (Mutation.createSubscription
                { input =
                    { clientMutationId = Absent
                    , subscription =
                        { id = Absent
                        , data = Present (encodeSubscriptionSet data)
                        , created = Absent
                        }
                    }
                }
                createPayloadSelection
            )


update : UUID -> SubscriptionSet -> SelectionSet (Maybe String) RootMutation
update (UUID id) data =
    Mutation.selection Maybe.Extra.join
        |> with
            (Mutation.updateSubscriptionById
                { input =
                    { clientMutationId = Absent
                    , id = id
                    , subscriptionPatch =
                        { id = Absent
                        , data = Present (Data.Source.encodeSubscriptionSet data)
                        , created = Absent
                        }
                    }
                }
                updatePayloadSelection
            )


updatePayloadSelection : SelectionSet (Maybe String) UpdateSubscriptionPayload
updatePayloadSelection =
    UpdateSubscriptionPayload.selection identity
        |> with
            (UpdateSubscriptionPayload.subscription subscriptionCreateSelection)


createPayloadSelection : SelectionSet (Maybe String) CreateSubscriptionPayload
createPayloadSelection =
    CreateSubscriptionPayload.selection identity
        |> with
            (CreateSubscriptionPayload.subscription subscriptionCreateSelection)


subscriptionCreateSelection : SelectionSet String ScraperData.Object.Subscription
subscriptionCreateSelection =
    Subscription.selection identity
        |> with
            Subscription.id
