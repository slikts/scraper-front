module Route exposing (..)

import UrlParser as Url exposing ((</>), Parser, oneOf, parseHash, s, string)
import Data.Subscription as Subscription
import Navigation exposing (Location)
import Html exposing (Attribute)
import Html.Attributes as Attr


type Route
    = New
    | Edit String
    | Subscription String


route : Parser (Route -> a) a
route =
    oneOf
        [ Url.map New (s "")
        , Url.map Edit (s "edit" </> Subscription.idParser)
        , Url.map Subscription (s "sub" </> Subscription.idParser)
        ]


routeToString : Route -> String
routeToString page =
    let
        pieces =
            case page of
                New ->
                    []

                Subscription id ->
                    [ "sub", id ]

                Edit id ->
                    [ "edit", id ]
    in
        "#/" ++ String.join "/" pieces


fromLocation : Location -> Maybe Route
fromLocation location =
    if String.isEmpty location.hash then
        Just New
    else
        parseHash route location


href : Route -> Attribute msg
href route =
    Attr.href (routeToString route)


modifyUrl : Route -> Cmd msg
modifyUrl =
    routeToString >> Navigation.modifyUrl
