-- Do not manually edit this file, it was auto-generated by Graphqelm
-- https://github.com/dillonkearns/graphqelm


module ScraperData.Object.GenRandomBytesPayload exposing (..)

import Graphqelm.Field as Field exposing (Field)
import Graphqelm.Internal.Builder.Argument as Argument exposing (Argument)
import Graphqelm.Internal.Builder.Object as Object
import Graphqelm.Internal.Encode as Encode exposing (Value)
import Graphqelm.OptionalArgument exposing (OptionalArgument(Absent))
import Graphqelm.SelectionSet exposing (SelectionSet)
import Json.Decode as Decode
import ScraperData.InputObject
import ScraperData.Interface
import ScraperData.Object
import ScraperData.Scalar
import ScraperData.Union


{-| Select fields to build up a SelectionSet for this object.
-}
selection : (a -> constructor) -> SelectionSet (a -> constructor) ScraperData.Object.GenRandomBytesPayload
selection constructor =
    Object.selection constructor


{-| The exact same `clientMutationId` that was provided in the mutation input, unchanged and unused. May be used by a client to track mutations.
-}
clientMutationId : Field (Maybe String) ScraperData.Object.GenRandomBytesPayload
clientMutationId =
    Object.fieldDecoder "clientMutationId" [] (Decode.string |> Decode.nullable)


string : Field (Maybe String) ScraperData.Object.GenRandomBytesPayload
string =
    Object.fieldDecoder "string" [] (Decode.string |> Decode.nullable)


{-| Our root query field type. Allows us to run any query from our mutation payload.
-}
query : SelectionSet decodesTo RootQuery -> Field (Maybe decodesTo) ScraperData.Object.GenRandomBytesPayload
query object =
    Object.selectionField "query" [] object (identity >> Decode.nullable)
