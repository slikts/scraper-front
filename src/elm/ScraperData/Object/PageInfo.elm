-- Do not manually edit this file, it was auto-generated by Graphqelm
-- https://github.com/dillonkearns/graphqelm


module ScraperData.Object.PageInfo exposing (..)

import Graphqelm.Field as Field exposing (Field)
import Graphqelm.Internal.Builder.Argument as Argument exposing (Argument)
import Graphqelm.Internal.Builder.Object as Object
import Graphqelm.Internal.Encode as Encode exposing (Value)
import Graphqelm.OptionalArgument exposing (OptionalArgument(Absent))
import Graphqelm.SelectionSet exposing (SelectionSet)
import Json.Decode as Decode
import ScraperData.InputObject
import ScraperData.Interface
import ScraperData.Object
import ScraperData.Scalar
import ScraperData.Union


{-| Select fields to build up a SelectionSet for this object.
-}
selection : (a -> constructor) -> SelectionSet (a -> constructor) ScraperData.Object.PageInfo
selection constructor =
    Object.selection constructor


{-| When paginating forwards, are there more items?
-}
hasNextPage : Field Bool ScraperData.Object.PageInfo
hasNextPage =
    Object.fieldDecoder "hasNextPage" [] Decode.bool


{-| When paginating backwards, are there more items?
-}
hasPreviousPage : Field Bool ScraperData.Object.PageInfo
hasPreviousPage =
    Object.fieldDecoder "hasPreviousPage" [] Decode.bool


{-| When paginating backwards, the cursor to continue.
-}
startCursor : Field (Maybe ScraperData.Scalar.Cursor) ScraperData.Object.PageInfo
startCursor =
    Object.fieldDecoder "startCursor" [] (Decode.oneOf [ Decode.string, Decode.float |> Decode.map toString, Decode.int |> Decode.map toString, Decode.bool |> Decode.map toString ] |> Decode.map ScraperData.Scalar.Cursor |> Decode.nullable)


{-| When paginating forwards, the cursor to continue.
-}
endCursor : Field (Maybe ScraperData.Scalar.Cursor) ScraperData.Object.PageInfo
endCursor =
    Object.fieldDecoder "endCursor" [] (Decode.oneOf [ Decode.string, Decode.float |> Decode.map toString, Decode.int |> Decode.map toString, Decode.bool |> Decode.map toString ] |> Decode.map ScraperData.Scalar.Cursor |> Decode.nullable)
