-- Do not manually edit this file, it was auto-generated by Graphqelm
-- https://github.com/dillonkearns/graphqelm


module ScraperData.Object.Source exposing (..)

import Graphqelm.Field as Field exposing (Field)
import Graphqelm.Internal.Builder.Argument as Argument exposing (Argument)
import Graphqelm.Internal.Builder.Object as Object
import Graphqelm.Internal.Encode as Encode exposing (Value)
import Graphqelm.OptionalArgument exposing (OptionalArgument(Absent))
import Graphqelm.SelectionSet exposing (SelectionSet)
import Json.Decode as Decode
import ScraperData.Enum.ItemsOrderBy
import ScraperData.InputObject
import ScraperData.Interface
import ScraperData.Object
import ScraperData.Scalar
import ScraperData.Union


{-| Select fields to build up a SelectionSet for this object.
-}
selection : (a -> constructor) -> SelectionSet (a -> constructor) ScraperData.Object.Source
selection constructor =
    Object.selection constructor


{-| A globally unique identifier. Can be used in various places throughout the system to identify this single value.
-}
nodeId : Field ScraperData.Scalar.Id ScraperData.Object.Source
nodeId =
    Object.fieldDecoder "nodeId" [] (Decode.oneOf [ Decode.string, Decode.float |> Decode.map toString, Decode.int |> Decode.map toString, Decode.bool |> Decode.map toString ] |> Decode.map ScraperData.Scalar.Id)


name : Field String ScraperData.Object.Source
name =
    Object.fieldDecoder "name" [] Decode.string


url : Field String ScraperData.Object.Source
url =
    Object.fieldDecoder "url" [] Decode.string


type alias ItemsBySourceOptionalArguments =
    { orderBy : OptionalArgument ScraperData.Enum.ItemsOrderBy.ItemsOrderBy, before : OptionalArgument ScraperData.Scalar.Cursor, after : OptionalArgument ScraperData.Scalar.Cursor, first : OptionalArgument Int, last : OptionalArgument Int, offset : OptionalArgument Int, condition : OptionalArgument ScraperData.InputObject.ItemCondition }


{-| Reads and enables pagination through a set of `Item`.

  - orderBy - The method to use when ordering `Item`.
  - before - Read all values in the set before (above) this cursor.
  - after - Read all values in the set after (below) this cursor.
  - first - Only read the first `n` values of the set.
  - last - Only read the last `n` values of the set.
  - offset - Skip the first `n` values from our `after` cursor, an alternative to cursor based pagination. May not be used with `last`.
  - condition - A condition to be used in determining which values should be returned by the collection.

-}
itemsBySource : (ItemsBySourceOptionalArguments -> ItemsBySourceOptionalArguments) -> SelectionSet decodesTo ScraperData.Object.ItemsConnection -> Field (Maybe decodesTo) ScraperData.Object.Source
itemsBySource fillInOptionals object =
    let
        filledInOptionals =
            fillInOptionals { orderBy = Absent, before = Absent, after = Absent, first = Absent, last = Absent, offset = Absent, condition = Absent }

        optionalArgs =
            [ Argument.optional "orderBy" filledInOptionals.orderBy (Encode.enum ScraperData.Enum.ItemsOrderBy.toString), Argument.optional "before" filledInOptionals.before (\(ScraperData.Scalar.Cursor raw) -> Encode.string raw), Argument.optional "after" filledInOptionals.after (\(ScraperData.Scalar.Cursor raw) -> Encode.string raw), Argument.optional "first" filledInOptionals.first Encode.int, Argument.optional "last" filledInOptionals.last Encode.int, Argument.optional "offset" filledInOptionals.offset Encode.int, Argument.optional "condition" filledInOptionals.condition ScraperData.InputObject.encodeItemCondition ]
                |> List.filterMap identity
    in
    Object.selectionField "itemsBySource" optionalArgs object (identity >> Decode.nullable)
