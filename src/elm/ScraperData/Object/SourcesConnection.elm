-- Do not manually edit this file, it was auto-generated by Graphqelm
-- https://github.com/dillonkearns/graphqelm


module ScraperData.Object.SourcesConnection exposing (..)

import Graphqelm.Field as Field exposing (Field)
import Graphqelm.Internal.Builder.Argument as Argument exposing (Argument)
import Graphqelm.Internal.Builder.Object as Object
import Graphqelm.Internal.Encode as Encode exposing (Value)
import Graphqelm.OptionalArgument exposing (OptionalArgument(Absent))
import Graphqelm.SelectionSet exposing (SelectionSet)
import Json.Decode as Decode
import ScraperData.InputObject
import ScraperData.Interface
import ScraperData.Object
import ScraperData.Scalar
import ScraperData.Union


{-| Select fields to build up a SelectionSet for this object.
-}
selection : (a -> constructor) -> SelectionSet (a -> constructor) ScraperData.Object.SourcesConnection
selection constructor =
    Object.selection constructor


{-| Information to aid in pagination.
-}
pageInfo : SelectionSet decodesTo ScraperData.Object.PageInfo -> Field decodesTo ScraperData.Object.SourcesConnection
pageInfo object =
    Object.selectionField "pageInfo" [] object identity


{-| The count of _all_ `Source` you could get from the connection.
-}
totalCount : Field (Maybe Int) ScraperData.Object.SourcesConnection
totalCount =
    Object.fieldDecoder "totalCount" [] (Decode.int |> Decode.nullable)


{-| A list of edges which contains the `Source` and cursor to aid in pagination.
-}
edges : SelectionSet decodesTo ScraperData.Object.SourcesEdge -> Field (Maybe (List (Maybe decodesTo))) ScraperData.Object.SourcesConnection
edges object =
    Object.selectionField "edges" [] object (identity >> Decode.nullable >> Decode.list >> Decode.nullable)


{-| A list of `Source` objects.
-}
nodes : SelectionSet decodesTo ScraperData.Object.Source -> Field (Maybe (List decodesTo)) ScraperData.Object.SourcesConnection
nodes object =
    Object.selectionField "nodes" [] object (identity >> Decode.list >> Decode.nullable)
