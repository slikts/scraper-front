module Views.Edit exposing (..)

import Data.Source exposing (..)
import Dict
import Html exposing (..)
import Html.Attributes as Attributes exposing (attribute, class, classList, href, id, placeholder, src, style, type_)
import Html.Events exposing (onCheck, onClick)
import Page.Edit exposing (Model, Msg(Create, ToggleSource, ToggleSub, Update))
import Route
import Set exposing (Set)


view : Model -> Html Msg
view { id, data, subs, visibleSources } =
    div []
        [ h2 []
            (if String.isEmpty id then
                [ text "New" ]
             else
                [ text "Edit "
                , a [ Route.href (Route.Subscription id) ] [ text id ]
                ]
            )
        , if String.isEmpty id then
            button [ onClick Create ] [ text "Save" ]
          else
            button [ onClick Update ] [ text "Save" ]
        , data
            |> Data.Source.toDict
            |> viewData (Data.Source.toDict subs) visibleSources
        ]


viewData :
    SourceGroupDict
    -> Set Source
    -> SourceGroupDict
    -> Html Msg
viewData subs visibleSources data =
    div []
        [ viewSourceList subs visibleSources data
        ]


sourceLabel : Source -> Bool -> Html Msg
sourceLabel source visible =
    button [ onClick (ToggleSource source) ]
        [ text
            ((if visible then
                "-"
              else
                "+"
             )
                ++ " "
                ++ source
            )
        ]


viewSourceList :
    SourceGroupDict
    -> Set Source
    -> SourceGroupDict
    -> Html Msg
viewSourceList subs visibleSources data =
    Dict.keys data
        |> List.map
            (\source ->
                let
                    visible =
                        Set.member source visibleSources
                in
                li []
                    [ sourceLabel source visible
                    , viewGroupList source subs data visible
                    ]
            )
        |> ol []


viewGroupList source subs data visible =
    Dict.get source data
        |> Maybe.withDefault Set.empty
        |> Set.toList
        |> List.map (viewGroup subs source)
        |> ol
            [ style
                (if visible then
                    []
                 else
                    [ ( "display", "none" ) ]
                )
            ]


checkbox : String -> String -> Bool -> Html Msg
checkbox source group checked =
    label
        []
        [ input [ type_ "checkbox", Attributes.checked checked, onCheck (ToggleSub ( source, group )) ] []
        , text group
        ]


viewGroup : SourceGroupDict -> Data.Source.Source -> Data.Source.Group -> Html Msg
viewGroup subs source group =
    li [] [ checkbox source group (Data.Source.checkIfSubbed subs source group) ]
