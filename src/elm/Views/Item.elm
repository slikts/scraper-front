module Views.Item exposing (..)

import Data.Item exposing (ItemResponse)
import RemoteData exposing (RemoteData)
import Html exposing (Html, a, div, h1, h2, img, p, pre, text)


itemsView : RemoteData a ItemResponse -> Html msg
itemsView a =
    case a of
        RemoteData.NotAsked ->
            text "Initialising."

        RemoteData.Loading ->
            text "Loading."

        RemoteData.Failure err ->
            text ("Error: " ++ toString err)

        RemoteData.Success items ->
            div [] (List.map (\{ name, key, source } -> div [] [ text key ]) items)
