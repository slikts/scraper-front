module Views.Page exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Lazy exposing (lazy2)
import Util exposing ((=>))
import Views.Spinner exposing (spinner)
import Route


type ActivePage
    = Other
    | Edit
    | Subscription


frame : Bool -> a -> Html msg -> Html msg
frame isLoading page content =
    div [ class "page-frame" ]
        [ viewHeader isLoading
        , content
        , viewFooter
        ]


viewHeader : Bool -> Html msg
viewHeader isLoading =
    nav [ class "header" ]
        [ div [ class "container" ]
            [ h1 []
                [ a [ Route.href Route.New ] [ text "scraper-front" ]
                ]
            , lazy2 Util.viewIf isLoading spinner
            ]
        ]


viewFooter : Html msg
viewFooter =
    footer []
        [ div [ class "container" ]
            [ text "🍍" ]
        ]
