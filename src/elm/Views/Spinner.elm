module Views.Spinner exposing (spinner)

import Html exposing (Attribute, Html, div, li)


spinner : Html msg
spinner =
    div [] [ Html.text "loading" ]
