module Views.Subscription exposing (..)

import Data.Item exposing (Item)
import Date exposing (Date)
import Date.Format
import Html exposing (..)
import Html.Attributes as Attributes exposing (attribute, class, classList, href, id, placeholder, rel, src, type_)
import Page.Subscription exposing (Model)
import Route


formatDate : Date -> String
formatDate =
    Date.Format.format "%Y-%m-%d"


view : Model -> Html msg
view { id, data } =
    case data of
        [] ->
            div [] [ text "No data" ]

        _ ->
            section []
                [ h2 []
                    [ text id
                    , text " "
                    , a [ Route.href (Route.Edit id) ] [ text "Edit" ]
                    ]
                , table
                    [ class "edit" ]
                    (List.map viewItem data)
                ]


viewItem : Item -> Html msg
viewItem { name, key, source, time } =
    tr []
        [ td [ class "item-time" ] [ text (formatDate time) ]
        , td [] [ text source ]
        , td [] [ a [ href key, rel "noreferrer" ] [ text name ] ]
        ]
