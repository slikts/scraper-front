// pull in desired CSS/SASS files
// require( './styles/main.scss' );
// var $ = jQuery = require( '../../node_modules/jquery/dist/jquery.js' );           // <--- remove if jQuery not needed
// require( '../../node_modules/bootstrap-sass/assets/javascripts/bootstrap.js' );   // <--- remove if Bootstrap's JS not needed

// inject bundled Elm app into div#main
var Elm = require( '../elm/Main' );
// Elm.Main.embed( document.getElementById( 'main' ) );

var app = Elm.Main.fullscreen();

// app.ports.storeSession.subscribe(function(session) {
//   localStorage.session = session;
// });

// window.addEventListener("storage", function(event) {
//   if (event.storageArea === localStorage && event.key === "session") {
//     app.ports.onSessionChange.send(event.newValue);
//   }
// }, false);
